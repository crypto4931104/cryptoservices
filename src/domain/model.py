from dataclasses import dataclass
import datetime


@dataclass(unsafe_hash=True)
class CryptocurrencyValue:
    # A value object to represent the price and volume ranking of a cryptocurrency.
    symbol: str
    price_USD: float
    rank: int


class CryptocurrencyValueBatch:
    # An entity to represent a timestamped collection of cryptocurrency values.
    def __init__(self, data: list[CryptocurrencyValue], timestamp: datetime.datetime):
        self.utc_timestamp = timestamp
        self.cryptocurrency_data = data[:]

    @property
    def timestamp(self):
        return self.utc_timestamp

    @property
    def data(self):
        return self.cryptocurrency_data[:]
