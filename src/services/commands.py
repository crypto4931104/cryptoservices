import datetime
from domain.model import CryptocurrencyValueBatch
from ports.repository import AbstractRepository
from ports.source import AbstractDataSourceClient


def obtain_utc_minutes_timestamp() -> datetime.datetime:
    # A service function to generate UTC timestamps with minutes resolution.
    return datetime.datetime.now(tz=datetime.UTC).replace(second=0, microsecond=0)


def get_batch_from_repository(timestamp: datetime.datetime, repository: AbstractRepository):
    return repository.get(timestamp=timestamp)


def add_batch_to_repository(batch: CryptocurrencyValueBatch, repository: AbstractRepository):
    return repository.add(batch)


def get_fresh_batch(client: AbstractDataSourceClient):
    return client.get()

