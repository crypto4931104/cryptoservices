from datetime import datetime

from domain.model import CryptocurrencyValueBatch
from ports.repository import AbstractRepository


class MongoRepository(AbstractRepository):
    # A MongoDB-based repository adapter for persistence.

    def __init__(self):
        pass

    def add(self, batch: CryptocurrencyValueBatch):
        pass

    def get(self, timestamp: datetime):
        pass

    def __contains__(self, timestamp: datetime):
        pass


class RedisRepository(AbstractRepository):
    # A Redis-based repository adapter for caching.

    def __init__(self):
        pass

    def add(self, batch: CryptocurrencyValueBatch):
        pass

    def get(self, timestamp: datetime):
        pass

    def __contains__(self, timestamp: datetime):
        pass
