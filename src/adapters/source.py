from ports.source import AbstractDataSourceServer, AbstractDataSourceClient


class RabbitmqAsyncRpcServer(AbstractDataSourceServer):
    # A Rabbitmq RPC-based data source server adapter.

    def __init__(self):
        pass


class RabbitmqAsyncRpcClient(AbstractDataSourceClient):
    # A Rabbitmq RPC-based data source client adapter.

    def __init__(self):
        pass

    def get(self):
        pass
