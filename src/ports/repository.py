import abc
from datetime import datetime

from domain.model import CryptocurrencyValueBatch


class AbstractRepository(abc.ABC):
    # A port for cryptocurrency batch repositories.

    @abc.abstractmethod
    def add(self, batch: CryptocurrencyValueBatch):
        pass

    @abc.abstractmethod
    def get(self, timestamp: datetime):
        pass

    @abc.abstractmethod
    def __contains__(self, timestamp: datetime):
        pass
