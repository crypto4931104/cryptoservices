import abc


class AbstractDataSourceServer(abc.ABC):
    # A port for data servers.
    pass


class AbstractDataSourceClient(abc.ABC):
    # A port for data clients.

    @abc.abstractmethod
    def get(self):
        pass
