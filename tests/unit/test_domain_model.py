import datetime
import pytest

from domain import model


@pytest.mark.it('Instantiates CryptocurrencyValue objects.')
def test_class_cryptocurrency_value():
    symbol, price_usd, rank = 'BTC', 45_000.06, 1
    value = model.CryptocurrencyValue(symbol=symbol, price_USD=price_usd, rank=rank)
    assert value.symbol == 'BTC'
    assert value.price_USD == price_usd
    assert value.rank == rank


@pytest.mark.it('Instantiates CryptocurrencyValueBatch objects.')
def test_class_cryptocurrency_value_batch():
    timestamp = datetime.datetime.now(tz=datetime.UTC).replace(second=0, microsecond=0)
    cryptocurrency_values = [
        model.CryptocurrencyValue(symbol='BTC', price_USD=46_000.05, rank=1),
        model.CryptocurrencyValue(symbol='ETH', price_USD=9002.17, rank=2),
        model.CryptocurrencyValue(symbol='WTF', price_USD=0.00458, rank=3)
    ]
    batch = model.CryptocurrencyValueBatch(timestamp=timestamp, data=cryptocurrency_values)
    assert batch.timestamp == timestamp
    assert batch.data == cryptocurrency_values
    assert batch.data is not cryptocurrency_values




