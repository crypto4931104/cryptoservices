import datetime
import pytest

from domain import model
from services import commands
from ports.repository import AbstractRepository
from ports.source import AbstractDataSourceClient


class FakeRepository(AbstractRepository):

    def __init__(self):
        self.data = []

    def __contains__(self, timestamp):
        for item in self.data:
            if item.utc_timestamp == timestamp:
                return True
        return False

    def add(self, batch: model.CryptocurrencyValueBatch):
        self.data.append(batch)

    def get(self, timestamp):
        for item in self.data:
            if item.utc_timestamp == timestamp:
                return item


def make_fake_batch():
    return model.CryptocurrencyValueBatch(timestamp=commands.obtain_utc_minutes_timestamp(),
                                          data=[model.CryptocurrencyValue(symbol='BTC', price_USD=46_000.05, rank=1)])


fake_batch = make_fake_batch()
fake_repo = FakeRepository()


@pytest.mark.it('Generates UTC timestamps.')
def test_makes_utc_timestamps():
    timestamp = commands.obtain_utc_minutes_timestamp()
    assert timestamp.tzinfo is datetime.UTC


@pytest.mark.it('Generates timestamps with minutes resolution.')
def test_makes_minutes_timestamps():
    timestamp = commands.obtain_utc_minutes_timestamp()
    assert timestamp.second == 0 and timestamp.microsecond == 0


@pytest.mark.it('Adds batch to repository.')
def test_add_batch_to_repository():
    commands.add_batch_to_repository(fake_batch, fake_repo)
    assert len(fake_repo.data) == 1 and fake_repo.data[0] == fake_batch


@pytest.mark.it('Gets batch by timestamp from repository.')
def test_get_batch_from_repository():
    result = commands.get_batch_from_repository(fake_batch.timestamp, fake_repo)
    assert result == fake_batch


class FakeDataSourceClient(AbstractDataSourceClient):

    def get(self):
        return make_fake_batch()


fake_source_client = FakeDataSourceClient()


@pytest.mark.it('Gets fresh batch from data source client.')
def test_get_fresh_batch():
    result = commands.get_fresh_batch(fake_source_client)
    assert type(result) is model.CryptocurrencyValueBatch
